export const INIT_TIMER = 'INIT_TIMER';
export const START_TIMER = 'START_TIMER';
export const STOP_TIMER = 'STOP_TIMER';
export const RESET_TIMER = 'RESET_TIMER';
export const TICK_TIMER = 'TICK_TIMER';
export const FINISH_TIMER = 'FINISH_TIMER';
export const STOP_SOUND = 'STOP_SOUND';
